using System.Linq;
using Dapper;
using Microsoft.Data.Sqlite;

namespace API.Data
{
    public class RouletteDbConnection :IRouletteDbConnection
    {
        private readonly DatabaseConfig _config;
        public RouletteDbConnection(DatabaseConfig config)
        {
            _config= config;
        }
 
        public void Setup()
        {
            using var connection = new SqliteConnection(_config.Name);
 
            var table = connection.Query<string>("SELECT name FROM sqlite_master WHERE type='table' AND name = 'Bets';");
            var tableName = table.FirstOrDefault();
            if (!string.IsNullOrEmpty(tableName) && tableName == "Bets")
                return;
 
            connection.Execute("Create Table Bets("+
                "Id int NOT NULL PRIMARY KEY,"+
               " Ball int ,"+
                "Color varchar(25),"+
                "Stake money,"+
                "BetStatus varchar(25),"+
                "StartTime datetime);"
            );
             connection.Execute("CREATE TABLE Payouts("+
               " Id int NOT NULL PRIMARY KEY,"+
               " BetId int ,"+
                "PayoutAmount money,"+
                "FOREIGN KEY(BetId) REFERENCES Bets(Id));"
                
            );
              connection.Execute("CREATE TABLE Spins("+
                "Id int NOT NULL PRIMARY KEY,"+
                "BallOutcome int," +
                "ColorOutcome varchar(50),"+
                "Odds decimal," +
                "BetId int,"+
                "FOREIGN KEY(BetId) REFERENCES Bets(Id));"    
            );
        }
        
    }
}