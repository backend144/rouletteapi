using System.ComponentModel.DataAnnotations;

namespace API.Entities
{
    public class Bet
    {
        [Key]
        public int Id { get; set; }
        public int Ball { get; set; }
        public string Color { get; set; }
        public int Stake { get; set; }
        public string BetStatus { get; set; }
        public string StartTime { get; set; }
    }
}