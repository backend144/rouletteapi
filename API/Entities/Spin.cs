using System.ComponentModel.DataAnnotations;

namespace API.Entities
{
    public class Spin
    {
        [Key]
        public int Id { get; set; }
        public int BallOutcome { get; set; }
        public string ColorOutcome { get; set; }
        public double Odds { get; set; }
        public int BetId { get; set; }
        
    }
}