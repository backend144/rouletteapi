using System.ComponentModel.DataAnnotations;

namespace API.Entities
{
    public class Payouts
    {
        [Key]
        public int Id { get; set; }
        public int BetId { get; set; }
        public double PayoutAmount { get; set; }
    }
}