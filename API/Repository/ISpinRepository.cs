using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;

namespace API.Repository
{
    public interface ISpinRepository
    {
        Task<IEnumerable<Spin>> GetPreviousSpins();
        Task SpinWheel(Spin spin);
    }
}