using System.Collections.Generic;
using System.Threading.Tasks;
using API.Data;
using API.Entities;
using Dapper;
using Microsoft.Data.Sqlite;

namespace API.Repository
{
    public class SpinRepository : ISpinRepository
    {
        public readonly DatabaseConfig _config;

        public SpinRepository(DatabaseConfig config)
        {
            _config = config;
        }

        public async Task SpinWheel(Spin spin)
        {
             var query = "INSERT INTO Spins(Id,BallOutcome,ColorOutcome,Odds,BetId)" + "VALUES (@Id,@BallOutcome,@ColorOutcome,@Odds,@BetId);";

            using (var connection = new SqliteConnection(_config.Name))
            {
                connection.Open();
                await connection.ExecuteAsync(query, spin);
            }
        }

          public async Task<IEnumerable<Spin>> GetPreviousSpins()
        {
            var query = "SELECT Id,BallOutcome,ColorOutcome,Odds,BetId FROM Spins;";

            using (var connection = new SqliteConnection(_config.Name))
            {
                connection.Open();
                return await connection.QueryAsync<Spin>(query);
            }

        }
    }
}