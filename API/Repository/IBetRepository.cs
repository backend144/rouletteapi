using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;

namespace API.Repository
{
    public interface IBetRepository
    {
         Task PlaceBet(Bet bet);
         Task<IEnumerable<Bet>> GetBets();
    }
}