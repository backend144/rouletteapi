using System.Collections.Generic;
using System.Threading.Tasks;
using API.Data;
using API.Entities;
using Dapper;
using Microsoft.Data.Sqlite;

namespace API.Repository
{
    public class BetRepository : IBetRepository
    {
        private readonly DatabaseConfig _config;

        public BetRepository(DatabaseConfig config)
        {
            _config = config;
        }

        public async Task PlaceBet(Bet bet)
        {
            var query = "INSERT INTO Bets(Id,Ball,Color,Stake,BetStatus,StartTime)" + "VALUES (@Id,@Ball,@Color,@Stake,@BetStatus,@StartTime);";

            using (var connection = new SqliteConnection(_config.Name))
            {
                connection.Open();
                await connection.ExecuteAsync(query, bet);
            }
        }

        public async Task<IEnumerable<Bet>> GetBets()
        {
            var query = "SELECT Id,Ball,Color,Stake,BetStatus,StartTime FROM Bets;";

            using (var connection = new SqliteConnection(_config.Name))
            {
                connection.Open();
                return await connection.QueryAsync<Bet>(query);
            }

        }
    }
}