using System.Collections.Generic;
using System.Threading.Tasks;
using API.Data;
using API.Entities;
using Dapper;
using Microsoft.Data.Sqlite;

namespace API.Repository
{
    public class PayoutsRepository : IPayoutsRepository
    {
        public readonly DatabaseConfig _config;

        public PayoutsRepository(DatabaseConfig config)
        {
            _config = config;
        }

        public async Task PayBet(Payouts payouts)
        {
            var query = "INSERT INTO Payouts(Id,BetId,PayoutAmount)" + "VALUES (@Id,@BetId,@PayoutAmount);";

            using (var connection = new SqliteConnection(_config.Name))
            {
                connection.Open();
                await connection.ExecuteAsync(query, payouts);
            }
        }

        public async Task<IEnumerable<Payouts>> GetPayout()
        {
            var query = "SELECT Id,BetId,PayoutAmount FROM Payouts;";

            using (var connection = new SqliteConnection(_config.Name))
            {
                connection.Open();
                return await connection.QueryAsync<Payouts>(query);
            }

        }
    }
}