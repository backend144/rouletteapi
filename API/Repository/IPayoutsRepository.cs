using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;

namespace API.Repository
{
    public interface IPayoutsRepository
    {
        Task PayBet(Payouts payouts);
        Task<IEnumerable<Payouts>> GetPayout();
    }
}