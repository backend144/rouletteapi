using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;
using API.Repository;

namespace API.Services
{
    public class PayoutsService : IPayoutsService
    {
        private readonly IPayoutsRepository _payoutsRepository;
        
        public PayoutsService(IPayoutsRepository payoutsRepository)
        {
            _payoutsRepository = payoutsRepository;
        }

        public async Task<IEnumerable<Payouts>> GetPayout()
        {
            return await _payoutsRepository.GetPayout();
        }

        public async Task PayBet(Payouts payouts)
        {
            await _payoutsRepository.PayBet(payouts);
        }
        
    }
}