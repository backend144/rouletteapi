using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;

namespace API.Services
{
    public interface IPayoutsService
    {
        Task PayBet(Payouts payout);
        Task<IEnumerable<Payouts>> GetPayout();
    }
}