using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;
using API.Repository;

namespace API.Services
{
    public class SpinService : ISpinService
    {
        private readonly ISpinRepository _spinRepository;
        
        public SpinService(ISpinRepository spinRepository)
        {
            _spinRepository = spinRepository;
        }

        public async Task<IEnumerable<Spin>> GetPreviousSpins()
        {
            return await _spinRepository.GetPreviousSpins();
        }

        public async Task SpinWheel(Spin spin)
        {
            await _spinRepository.SpinWheel(spin);
        }
    }
}