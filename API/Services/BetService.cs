using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;
using API.Repository;
using PeanutButter.RandomGenerators;

namespace API.Services
{
    public class BetService : IBetService
    {
        private readonly IBetRepository _betRepository;
        private readonly ISpinRepository _spinRepository;
        private readonly IPayoutsRepository _payoutRepository;
        
        public BetService(IBetRepository betRepository, ISpinRepository spinRepository, IPayoutsRepository payoutRepository)
        {
            _betRepository = betRepository;
            _spinRepository = spinRepository;
            _payoutRepository = payoutRepository;
        }

        public async Task<IEnumerable<Bet>> GetBets()
        {
            return await _betRepository.GetBets();
        }

        public Bet GetBetStatus(Bet bet, Spin spin, Payouts payouts){
            bet.BetStatus = "Pending";        
            if (spin.BallOutcome == bet.Ball && spin.ColorOutcome.Equals(bet.Color))
            {
                bet.BetStatus = "Winning";
            }
            else if(bet.Ball < 0)
            {
             bet.BetStatus = "Invalid Bet";
            }
            else
            {
                bet.BetStatus = "Losing";
            }

            return bet;
        }

        public async Task PlaceBet(Bet bet)
        {
            var randomNumber = RandomValueGen.GetRandomInt(0, 36);

            var spin = new Spin
            {
               Id = RandomValueGen.GetRandomInt(1, 7000),
               BallOutcome = randomNumber,
               ColorOutcome = (randomNumber%2) == 0 ? "Red" : "Black",
               Odds = RandomValueGen.GetRandomDouble(0.5, 3.5),
               BetId = bet.Id  
            };

            var payouts = new Payouts
            {
                Id = RandomValueGen.GetRandomInt(1, 7000),
                BetId = bet.Id,
                PayoutAmount = bet.Stake * spin.Odds
            };

            var newBet  = GetBetStatus(bet, spin, payouts);
            await _betRepository.PlaceBet(bet);
            await _spinRepository.SpinWheel(spin);
            await _payoutRepository.PayBet(payouts); 
              
        }
    }
}