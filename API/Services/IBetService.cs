using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;

namespace API.Services
{
    public interface IBetService
    {
       Task<IEnumerable<Bet>> GetBets();
       Task PlaceBet(Bet bet);
    }
}