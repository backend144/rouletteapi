using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;

namespace API.Services
{
    public interface ISpinService
    {
         Task<IEnumerable<Spin>> GetPreviousSpins();
         Task SpinWheel(Spin spin);
    }
}