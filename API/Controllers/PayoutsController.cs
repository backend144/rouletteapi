using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;
using API.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PayoutsController : ControllerBase
    {
       private readonly IPayoutsService _payoutsService;

        public PayoutsController(IPayoutsService payoutsService)
        {
            _payoutsService = payoutsService;   
        }

        [HttpGet]
        public async Task<IEnumerable<Payouts>> GetPayout()
        {
            return await _payoutsService.GetPayout();
        }

        [HttpPost]
        public async Task PayBet([FromBody] Payouts payouts)
        {
            await _payoutsService.PayBet(payouts);
        }  
    }
}