using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;
using API.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpinController : ControllerBase
    {
        private readonly ISpinService _spinService;

        public SpinController(ISpinService spinService)
        {
            _spinService = spinService;   
        }

        [HttpGet]
        public async Task<IEnumerable<Spin>> GetPreviousSpins()
        {
            return await _spinService.GetPreviousSpins();
        }

        [HttpPost]
        public async Task PlaceBet([FromBody] Spin spin)
        {
            await _spinService.SpinWheel(spin);
        }
    }
}