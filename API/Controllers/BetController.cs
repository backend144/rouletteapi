using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;
using API.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BetController : ControllerBase
    {
        private readonly IBetService _betService;
        public BetController(IBetService betService)
        {
            _betService = betService;   
        }

        [HttpGet]
        public async Task<IEnumerable<Bet>> GetBets()
        {
            return await _betService.GetBets();
        }

        [HttpPost]
        public async Task PlaceBet([FromBody] Bet bet)
        {
            await _betService.PlaceBet(bet);
        }
    }
}