using System;
using Xunit;
using NSubstitute;
using System.Threading.Tasks;
using API.Repository;
using API.Services;
using API.Entities;

namespace RouletteApiTest
{
    public class BetServiceTests
    {
        [Fact]
        public async Task PlaceBet_SHOULD_ReturnBetStatus_WHEN_BetExist()
        {
            var _betRepository = Substitute.For<IBetRepository>();
            var _spinRepository = Substitute.For<ISpinRepository>();
            var _payoutRepository = Substitute.For<IPayoutsRepository>();

            var betService = new BetService(_betRepository, _spinRepository, _payoutRepository);

            var bet = new Bet{
                Id = 12,
                Ball = 23,
                Color = "Black",
                Stake = 1,
                BetStatus = "Pending",
                StartTime = "11"
            };

            var spin = new Spin
            {
               Id = 123,
               BallOutcome = 23,
               ColorOutcome = "Black",
               Odds = 2,
               BetId = bet.Id  
            };

            var payouts = new Payouts
            {
                Id = 4,
                BetId = bet.Id,
                PayoutAmount = bet.Stake * spin.Odds
            };


            var res = betService.GetBetStatus(bet, spin, payouts);

            
            Assert.Equal(res.BetStatus, "Winning");
        }
    }
}
